import { Component, OnInit,ViewChild } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ContactUsService } from 'src/app/shared/services/contact-us-services/contact-us.service';


/**
 * To get the data or post the data to the servoce and to arrage the data or handel all events of the action
 */
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  @ViewChild('formcontact', {static: false}) form; // To have the watch on the form anmd to make this reset after submit
  contactModel: any = {}; // to get all the conatct filled infor in the model
  constructor(public snakbar:MatSnackBar,public contactService:ContactUsService){ }

  ngOnInit() {
  }
  
  //when user clicks on contact us
  onSubmit() {
    this.contactService.submitConatctForm(this.contactModel)
    this.form.resetForm()// after submit form is reset
  }

}
