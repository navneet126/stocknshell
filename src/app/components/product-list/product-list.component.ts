import { Component, OnInit } from '@angular/core';
import { ProductListService } from 'src/app/shared/services/product-list-services/product-list.service';
import { GeneralService } from 'src/app/shared/services/utils/general.service';
import { MatSnackBar } from '@angular/material';

/**
 * To get the data or post the data to the servoce and to arrage the data or handel all events of the action
 */
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  allProductList: Array<any>
  productData; // get and store data from service to show the data
  pageOfItems: Array<any>; // for pagination
  pageNo: Number = 0;
  pageSize: Number = 5;
  totalPages: Number = 0;
  constructor(public generalservice:GeneralService,private snakbar:MatSnackBar) { }

  ngOnInit() {

    this.generalservice.getProductList().subscribe(productAllDetail=>{
      this.allProductList= productAllDetail["product"]
      this.totalPages=Math.round(this.allProductList.length/5)
      this.onPageChangeUpdate()
    }, err => {
      this.snakbar.open(err,"Close",{
      duration:4000
      })
    })
  }
  //when user changes the page
  onPageChange(event) {
    if (event) {
      this.pageNo = +event;
      this.onPageChangeUpdate();
    }
  }

  onPageChangeUpdate(){
    let startIndex = (+this.pageNo * +this.pageSize);
    this.pageOfItems = this.allProductList.slice(startIndex, (+startIndex + +this.pageSize));
  }

}
