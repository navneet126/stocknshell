import { Component, OnInit } from '@angular/core';

/**
 * To get the data or post the data to the servoce and to arrage the data or handel all events of the action
 * This is just for Page Not found
 */
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
