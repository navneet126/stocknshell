import { Component, OnInit } from '@angular/core';
/**
 * To get the data or post the data to the servoce and to arrage the data or handel all events of the action
 * This is just for the correct flow and make use of Nav bar
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
