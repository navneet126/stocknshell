import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule }   from '@angular/forms';
import { MatSnackBarModule } from "@angular/material";
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { GeneralService } from './shared/services/utils/general.service';
import { ContactUsService } from './shared/services/contact-us-services/contact-us.service';
import { ProductListService } from './shared/services/product-list-services/product-list.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagingComponent } from './shared/component/paging/paging.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ProductListComponent,
    ContactUsComponent,
    DashboardComponent,
    NotFoundComponent,
    PagingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatSnackBarModule,
    BrowserAnimationsModule

  ],
  providers: [GeneralService,ContactUsService,ProductListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
