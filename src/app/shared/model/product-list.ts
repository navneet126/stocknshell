/**
 * Model To get the only those thing from API which we needed else reject or ignore them
 */
export interface ProductList{
    name?:string,
    img?:string,
    lowest_ask?:string,
    highest_bid?:number,
    last_sale?:string,
    seo?:string,
    product_id?:string,
    brand?:string,
    category?:string,
    viewed?:number,
    date_available?:any,
    highest_bid_cad?:number,
    highest_bid_usd?:number,
    lowest_ask_cad?:string,
    lowest_ask_usd?:string
}
