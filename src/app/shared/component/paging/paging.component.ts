import { Component, OnInit, Input, Output, EventEmitter,OnChanges } from '@angular/core';

@Component({
  selector: 'ed-paging',
  templateUrl: 'paging.component.html'
})
export class PagingComponent implements OnInit ,OnChanges{
  @Input()
  pageNo: Number; // First Page nUmber
  @Input()
  pageSize: Number; // Total List to shown
  @Input()
  totalPages: Number; // Total number of pages
  pagingArray: Array<Number> = [];
  @Output()
  pageChange: EventEmitter<Number> = new EventEmitter<Number>();

  constructor() { }

  ngOnInit() {
    this.setPagingArray();
  }
  ngOnChanges(){
    this.setPagingArray();
  }


  onPageChange(mode, pageNo?) {
    if (mode === "pre") {
      this.pageNo = +this.pageNo - 1;
    }
    else if (mode === "next") {
      this.pageNo = +this.pageNo + 1;
    }
    else if (mode === "") {
      this.pageNo = +pageNo;
    }
    if (this.pageNo < 1)
      this.pageNo = 1;

    this.pageChange.emit(+this.pageNo);
    this.setPagingArray();
  }

  //to set the array when user chnages the page
  setPagingArray() {
    this.pagingArray = [];
    if (this.pageNo > 1) {
      for (let index = +this.pageNo - 1; index > 0; index--) {
        this.pagingArray.push(index);
        if (this.pagingArray.length == 2)
          break;
      }
    }

    this.pagingArray.push(this.pageNo);
    if (this.pageNo < this.totalPages) {
      for (let index = +this.pageNo + 1; index <= this.totalPages; index++) {
        this.pagingArray.push(index);
        if (this.pagingArray.length > 5)
          break;
      }
    }
  }
}
