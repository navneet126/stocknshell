import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError} from 'rxjs/operators';
import { ApiBaseUrlService } from '../apiurl/api-base-url.service';
import { ProductList } from '../../model/product-list';

/**
 * Service with HTTP methods
 */

 @Injectable({ providedIn: 'root' })

 /**
 * Use the ApiBaseUrlService to make the banse URL comman for the 
 * API so that no need to call that here and make the method here in the API call
 */
 export class GeneralService extends ApiBaseUrlService {
  constructor(protected http: HttpClient) {
    super(http);
  }
  

  //API call to Post the contact us form. You can send the Headers also usign the HttpParams 
  sendContactDetails(name, bodyOfForm, subject,email,contact) {
      const body = {
        name: name,
        body:bodyOfForm,
        subject:subject,
        email:email,
        contact:contact
      } 
    return this.http.post<string[]>(`${this.apiUrl}/Contact/submit`,body,{})
      .pipe(catchError(this.handleError));
  }
  /**
   * To get the Product list from the API and then response it 
   * the product lsit model you can also send the URL dynamic also as mentioned below.
   */
  getProductList() {
      return this.http.get<ProductList[]>(`${this.apiUrl}/product/search`, {})
      .pipe(catchError(this.handleError));
  }
}