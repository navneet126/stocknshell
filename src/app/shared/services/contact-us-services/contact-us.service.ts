import { Injectable } from '@angular/core';
import { GeneralService } from '../utils/general.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
/**
 * Service to post the contact form
 */
export class ContactUsService {

  constructor(public generalservice:GeneralService,private snakbar:MatSnackBar) { }
  /**
   * Method to submit the form data and all form data is passed in this method
   */
  submitConatctForm(formdata){
    //Let is used here insted of var 
    let name = formdata["fullNmae"]
    let contactNumber = formdata["contactNumber"]
    let message = formdata["message"]
    let email = formdata["email"]
    let subject = formdata["subject"]
    this.generalservice.sendContactDetails(name,message,subject,email,contactNumber).subscribe(response=>{
      this.snakbar.open(response["message"],"Close",{
        duration:4000
      })
    },err=>{
      this.snakbar.open(err,"Close",{
        duration:4000
      })
    })
  }
}
