import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs/internal/observable/throwError';

/**
 * This service is created to make the API Base URLS commman 
 * so that in future may add other API URl as testng and production API URL
 */
@Injectable({
  providedIn: 'root'
})
export class ApiBaseUrlService {

//Base API URL of stagging Can also add the Live URl then just need to comment and uncommnet this
protected apiUrl: string = "https://dev.llh.wwr.mybluehost.me/services/";
constructor(protected http: HttpClient) {
}

protected handleError = (error: HttpErrorResponse) => {
  if(error==undefined||error.error==undefined)
      return throwError('Cannot get the response')
    return throwError(error.error.message);
  };
}