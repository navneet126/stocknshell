import { TestBed } from '@angular/core/testing';

import { ApiBaseUrlService } from './api-base-url.service';

describe('ApiBaseUrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiBaseUrlService = TestBed.get(ApiBaseUrlService);
    expect(service).toBeTruthy();
  });
});
