import { Injectable, OnInit } from '@angular/core';
import { GeneralService } from '../utils/general.service';

@Injectable({
  providedIn: 'root'
})
/**
 * This Service is used to call the List of Product from the API for now as the task is to pnly show list
 * so no need for the other action to filter product by categories or to serarch the product. 
 */
export class ProductListService implements OnInit { 
  constructor(private generalService:GeneralService) { }

  ngOnInit(){
  }
  /**
   * Method to get all the product list if any error then error will be shown in snak bar 
   */
}
